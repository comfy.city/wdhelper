package wdhelper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

// run a wikidata query and return the json output as []byte
// code adapted from https://github.com/garyhouston/sparqlcmd
func RunWikidataQuery(query string) ([]byte, error) {
	form := url.Values{}
	form.Set("format", "json")
	form.Set("query", query)
	reqBody := bytes.NewBuffer([]byte(form.Encode()))
	req, err := http.NewRequest("POST", "https://query.wikidata.org/bigdata/namespace/wdq/sparql", reqBody)
	if err != nil {
		return []byte{}, err
	}
	req.Header.Add("User-Agent", "comfy-recipes")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, err
	}
	return body, nil
}

// run a wikidata query and return the output as a wJS struct
// T should be a struct with fields of type WItem and standard
// json directives with the variable names
func RunWikidataQuery2Struct[T any](query string) (wJS[T], error) {
	dat, err := RunWikidataQuery(query)
	if err != nil {
		return wJS[T]{}, err
	}

	var wjson wJS[T]

	log.Print("parsing json query result")
	err = json.Unmarshal(dat, &wjson)
	if err != nil {
		return wJS[T]{}, err
	}
	return wjson, err
}

func ParseWikidataID(str string) (int64, error) {
	IDstr := strings.TrimPrefix(str, "http://www.wikidata.org/entity/Q")
	id, err := strconv.ParseInt(IDstr, 10, 64)
	if err != nil {
		return -1, fmt.Errorf("%s should be in format http://www.wikidata.org/entity/Q<number>", str)
	}
	return id, nil
}

type wJS[T any] struct {
	Head    whead
	Results wresults[T] `json:"results"`
}

type whead struct {
	Vars []string
}

type wresults[T any] struct {
	Bindings []T `json:"bindings"`
}

type WItem struct {
	Type     string `json:"type"`
	Value    string `json:"value"`
	Xmllang  string `json:"xml:lang,omitempty"`
	Datatype string `json:"datatype,omitempty"`
}
